package com.ipan.okhttp;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ipan.okhttp.http.DebugLoggingInterceptor;

/**
 * 下载文件测试
 * 
 * @author iPan
 * @version 2019-12-30
 */
public class DownloadTest {
	
	private static final String URL = "http://localhost/zchj_app/app/http-test/download.jspx";
	
	@BeforeClass
	public static void init() {
		HttpClient.Instance.debugLog(DebugLoggingInterceptor.Level.ALL);
	}

	@Test
	public void download() {
		HttpClient.get(URL).asFile("d:/downloadFile.jpg");
	}
	
}
/*
2019-12-30 16:28:11,986 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Start to print the connection[http://localhost/zchj_app/app/http-test/download.jspx] data ===|
2019-12-30 16:28:11,987 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Start to print request headers === |
2019-12-30 16:28:11,987 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Accept-Language : zh-CN,zh;q=0.8 ===|
2019-12-30 16:28:11,987 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== User-Agent : Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36 ===|
2019-12-30 16:28:11,987 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Finish to print request headers === |
2019-12-30 16:28:12,087 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== The request executes over,http statusCode is 200,http message is OK,taking 96 ms
2019-12-30 16:28:12,087 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Start to print response headers === |
2019-12-30 16:28:12,087 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Content-Disposition : inline;filename= ===|
2019-12-30 16:28:12,087 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Content-Language : zh-CN ===|
2019-12-30 16:28:12,087 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Content-Length : 92094 ===|
2019-12-30 16:28:12,087 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Content-Type : image/jpeg;charset=UTF-8 ===|
2019-12-30 16:28:12,087 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Date : Mon, 30 Dec 2019 08:28:12 GMT ===|
2019-12-30 16:28:12,087 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Server : Apache-Coyote/1.1 ===|
2019-12-30 16:28:12,087 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Set-Cookie : JSESSIONID=A05FC7E62C989B9EC4CDB1CB5C6A67D5; Path=/zchj_app; HttpOnly ===|
2019-12-30 16:28:12,087 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Finish to print response headers === |
2019-12-30 16:28:12,087 [main] WARN  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== The response body may contains 'file' part, ignore to print! ===|
2019-12-30 16:28:12,087 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Finish to print the connection[http://localhost/zchj_app/app/http-test/download.jspx] data ===|
*/

