package com.ipan.okhttp;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ipan.okhttp.http.DebugLoggingInterceptor;

/**
 * 文本提交测试
 * 
 * @author iPan
 * @version 2019-12-30
 */
public class TextTest {// 文本包含：text、html、xml、json
	
	private static final String TEXT_URL = "http://localhost/zchj_app/app/http-test/textTest.textx";
	
	@BeforeClass
	public static void init() {
		HttpClient.Instance.debugLog(DebugLoggingInterceptor.Level.ALL);
	}

	@Test
	public void textTest() {
		String data = HttpClient
				.textBody(TEXT_URL)
                .queryString("id", "999")
                .queryString("name", "张三")
                .text("啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊\r\naaaaaaaaaaaaaaaaaaaaaa")
                .asString();
		System.out.println("data=" + data);
	}
	
	@Test
	public void htmlTest() {
		String data = HttpClient
				.textBody(TEXT_URL)
                .queryString("id", "999")
                .queryString("name", "张三")
                .html("<html><head></head><body>hello</body></html>")
                .asString();
		System.out.println("data=" + data);
	}
	
	@Test
	public void xmlTest() {
		String data = HttpClient
				.textBody(TEXT_URL)
                .queryString("id", "999")
                .queryString("name", "张三")
                .xml("<xml><sex>男</sex></xml>")
                .asString();
		System.out.println("data=" + data);
	}
	
	@Test
	public void jsonText() {
		String data = HttpClient
				.textBody(TEXT_URL)
                .queryString("id", "999")
                .queryString("name", "张三")
                .json("{\"a\":1, \"b\":\"bbb\"}")
                .asString();
		System.out.println("data=" + data);
	}
	
}
/*
textTest:
2019-12-30 15:58:51,041 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Start to print the connection[http://localhost/zchj_app/app/http-test/textTest.textx?id=999&name=%E5%BC%A0%E4%B8%89] data ===|
2019-12-30 15:58:51,042 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Start to print request headers === |
2019-12-30 15:58:51,043 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Accept-Language : zh-CN,zh;q=0.8 ===|
2019-12-30 15:58:51,043 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== User-Agent : Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36 ===|
2019-12-30 15:58:51,043 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Finish to print request headers === |
2019-12-30 15:58:51,043 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Start to print request body === |
2019-12-30 15:58:51,043 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== The body data is 啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊
aaaaaaaaaaaaaaaaaaaaaa ===|
2019-12-30 15:58:51,043 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Finish to print request body === |
2019-12-30 15:58:51,145 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== The request executes over,http statusCode is 200,http message is OK,taking 99 ms
2019-12-30 15:58:51,145 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Start to print response headers === |
2019-12-30 15:58:51,145 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Content-Language : zh-CN ===|
2019-12-30 15:58:51,145 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Content-Length : 18 ===|
2019-12-30 15:58:51,145 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Content-Type : text/plain;charset=utf-8 ===|
2019-12-30 15:58:51,146 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Date : Mon, 30 Dec 2019 07:58:51 GMT ===|
2019-12-30 15:58:51,146 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Server : Apache-Coyote/1.1 ===|
2019-12-30 15:58:51,146 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Set-Cookie : JSESSIONID=3D11ACE2D222C5B35873682B30CE3C82; Path=/zchj_app; HttpOnly ===|
2019-12-30 15:58:51,146 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Finish to print response headers === |
2019-12-30 15:58:51,146 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Start to print response body === |
2019-12-30 15:58:51,146 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== The body data is 
文本提交完成
2019-12-30 15:58:51,146 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Finish to print response body === |
2019-12-30 15:58:51,146 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Finish to print the connection[http://localhost/zchj_app/app/http-test/textTest.textx?id=999&name=%E5%BC%A0%E4%B8%89] data ===|

data=文本提交完成
*/
