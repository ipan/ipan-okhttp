package com.ipan.okhttp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ipan.okhttp.http.DebugLoggingInterceptor;

/**
 * 上传文件测试
 * 
 * 上传有两种：
 * 1 类似表单提交文件，上传文件会指明文件名；常用；
 * 2 直接提交二进制数据流，服务端读取文件流，没有指定文件名；这种不常用；
 * 
 * @author iPan
 * @version 2019-12-30
 */
public class UploadTest {
	
	private static final String URL = "http://localhost/zchj_app/app/http-test/upload.jsonx";
	
	@BeforeClass
	public static void init() {
		HttpClient.Instance.debugLog(DebugLoggingInterceptor.Level.ALL);
	}

	@Test
	public void uplaod() {
		Map<String, Object> retMap = HttpClient
                .post(URL) 
                .param("file", new File("d:/capital_05.jpg")) // 具体文件；
                .param("fileName", "capital_05.jpg") // 看服务端需要，提交了文件名；
                .param("id", "1") // 其他参数
                .asMap();
		System.out.println(retMap);
	}
	
	@Test
	public void uplaod2() {
		FileInputStream fin = null;
		Map<String, Object> retMap = null;
		try {
			fin = new FileInputStream(new File("d:/capital_05.jpg"));
			retMap = HttpClient
			        .post(URL) 
			        .param("file", fin, "capital_05.jpg")
			        .param("fileName", "capital_05.jpg")
			        .param("id", "1")
			        .asMap();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (fin != null) {
				try {
					fin.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		System.out.println(retMap);
	}
}
/*
uplaod测试
嗅探器抓包可见如下，将参数分成三部分提交，分别是fileName、id、file；file提交的时候指定的文件名使用filename="capital_05.jpg"标记；
listening on [any] 80 ...
connect to [127.0.0.1] from LAPTOP-IUTJ548T [127.0.0.1] 58549
POST /zchj_app/app/http-test/upload.jsonx HTTP/1.1
Accept-Language: zh-CN,zh;q=0.8
User-Agent: Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36
Content-Type: multipart/form-data; boundary=65063b55-6003-4a77-9f84-5c0669b3cdc7
Content-Length: 92535
Host: localhost
Connection: Keep-Alive
Accept-Encoding: gzip

--65063b55-6003-4a77-9f84-5c0669b3cdc7
Content-Disposition: form-data; name="fileName"
Content-Length: 14

capital_05.jpg
--65063b55-6003-4a77-9f84-5c0669b3cdc7
Content-Disposition: form-data; name="id"
Content-Length: 1

1
--65063b55-6003-4a77-9f84-5c0669b3cdc7
Content-Disposition: form-data; name="file"; filename="capital_05.jpg"
Content-Type: image/jpeg
Content-Length: 92094
下面是二进制的文件内容，略。。。
--65063b55-6003-4a77-9f84-5c0669b3cdc7--
-4a77-9f84-5c0669b3cdc7--
 sent 0, rcvd 92918
*/