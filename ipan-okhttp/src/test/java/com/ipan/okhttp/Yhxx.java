package com.ipan.okhttp;

public class Yhxx { // Test Bean
	private String statusCode = null;
	private String message = null;
	private Integer id;
	private String xm;
	private String zh;

	public Integer getId() {
		return id;
	}

	public String getXm() {
		return xm;
	}

	public String getZh() {
		return zh;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setXm(String xm) {
		this.xm = xm;
	}

	public void setZh(String zh) {
		this.zh = zh;
	}
	
	public String getStatusCode() {
		return statusCode;
	}

	public String getMessage() {
		return message;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "Yhxx [statusCode=" + statusCode + ", message=" + message + ", id=" + id + ", xm=" + xm + ", zh=" + zh + "]";
	}

}
