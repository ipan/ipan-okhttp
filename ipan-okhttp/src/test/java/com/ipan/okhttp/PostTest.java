package com.ipan.okhttp;

import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ipan.okhttp.http.DebugLoggingInterceptor;

/**
 * 普通POST测试
 * 
 * @author iPan
 * @version 2019-12-30
 */
public class PostTest {
	
	private static final String URL = "http://localhost/zchj_app/app/http-test/postTest.jsonx";
	
	@BeforeClass
	public static void init() {
		HttpClient.Instance.debugLog(DebugLoggingInterceptor.Level.ALL);
	}

	@Test
	public void postAsString() {
		String data = HttpClient
                .post(URL)
                .param("id", "888")
                .param("name", "张三")
                .asString();
		System.out.println("data=" + data);
	}
	
	@Test
	public void postAsMap() {
		Map<String, Object> map = HttpClient
				.post(URL)
                .param("id", "888")
                .param("name", "张三")
                .asMap();
		System.out.println("map=" + map);
	}
	
	@Test
	public void postAsBean() {
		Yhxx yhxx = HttpClient
				.post(URL)
                .param("id", "888")
                .param("name", "张三")
                .param("arr", "aaa")
                .param("arr", "bbb")
                .asBean(Yhxx.class); // 支持提交arr=[aaa, bbb]
		System.out.println("yhxx=" + yhxx);
	}
	
}
/*
postAsString:queryString方法是在url后面加参数，param是在post提交的时候加参数；
2019-12-30 15:25:59,510 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Start to print the connection[http://localhost/zchj_app/app/http-test/postTest.jsonx] data ===|
2019-12-30 15:25:59,511 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Start to print request headers === |
2019-12-30 15:25:59,512 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Accept-Language : zh-CN,zh;q=0.8 ===|
2019-12-30 15:25:59,512 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== User-Agent : Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36 ===|
2019-12-30 15:25:59,512 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Finish to print request headers === |
2019-12-30 15:25:59,512 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Start to print request body === |
2019-12-30 15:25:59,512 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== The body data is id=888&name=%E5%BC%A0%E4%B8%89 ===|
2019-12-30 15:25:59,512 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Finish to print request body === |
2019-12-30 15:25:59,617 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== The request executes over,http statusCode is 200,http message is OK,taking 103 ms
2019-12-30 15:25:59,617 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Start to print response headers === |
2019-12-30 15:25:59,617 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Content-Language : zh-CN ===|
2019-12-30 15:25:59,617 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Content-Length : 878 ===|
2019-12-30 15:25:59,617 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Content-Type : text/html;charset=utf-8 ===|
2019-12-30 15:25:59,617 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Date : Mon, 30 Dec 2019 07:25:59 GMT ===|
2019-12-30 15:25:59,617 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Server : Apache-Coyote/1.1 ===|
2019-12-30 15:25:59,617 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Set-Cookie : JSESSIONID=A4FBEA65865486203AA9F8CA2614AB34; Path=/zchj_app; HttpOnly ===|
2019-12-30 15:25:59,617 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Finish to print response headers === |
2019-12-30 15:25:59,617 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Start to print response body === |
2019-12-30 15:25:59,618 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== The body data is 
{"createDate":null,"createTime":null,"updateDate":null,"updateTime":null,"statusCode":"200","message":"操作成功","image":null,"id":888,"zh":"zhangsan","kl":null,"qx":null,"nc":null,"xm":"张三","xb":null,"cs":null,"whcd":null,"sfzh":null,"ysr":null,"szhy":null,"yx":null,"sj":null,"qq":null,"wxuid":null,"qquid":null,"wx":null,"wb":null,"tb":null,"xq":null,"zcsj":null,"lx":null,"bz":null,"fwq":null,"tpml":null,"dlsj":null,"dlcs":null,"khid":null,"tp0":null,"tp1":null,"tp2":null,"tp3":null,"tp4":null,"qy":null,"gxsj":null,"wxoid":null,"wxoid1":null,"wxoid2":null,"wxoid3":null,"wxoid4":null,"aliuid":null,"alilogin":null,"aliname":null,"szd":null,"szdName":null,"xgr":null,"xgsj":null,"tjr":null,"ddsj1":null,"uuid":null,"lastAppVer":null,"lastResVer":null,"platform":null,"sjsId":null,"sjsZt":null,"sfrzId":null,"sfrzZt":null,"jgrzId":null,"jgrzZt":null,"szdKhid":null}
2019-12-30 15:25:59,618 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Finish to print response body === |
2019-12-30 15:25:59,618 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Finish to print the connection[http://localhost/zchj_app/app/http-test/postTest.jsonx] data ===|

data={"createDate":null,"createTime":null,"updateDate":null,"updateTime":null,"statusCode":"200","message":"操作成功","image":null,"id":888,"zh":"zhangsan","kl":null,"qx":null,"nc":null,"xm":"张三","xb":null,"cs":null,"whcd":null,"sfzh":null,"ysr":null,"szhy":null,"yx":null,"sj":null,"qq":null,"wxuid":null,"qquid":null,"wx":null,"wb":null,"tb":null,"xq":null,"zcsj":null,"lx":null,"bz":null,"fwq":null,"tpml":null,"dlsj":null,"dlcs":null,"khid":null,"tp0":null,"tp1":null,"tp2":null,"tp3":null,"tp4":null,"qy":null,"gxsj":null,"wxoid":null,"wxoid1":null,"wxoid2":null,"wxoid3":null,"wxoid4":null,"aliuid":null,"alilogin":null,"aliname":null,"szd":null,"szdName":null,"xgr":null,"xgsj":null,"tjr":null,"ddsj1":null,"uuid":null,"lastAppVer":null,"lastResVer":null,"platform":null,"sjsId":null,"sjsZt":null,"sfrzId":null,"sfrzZt":null,"jgrzId":null,"jgrzZt":null,"szdKhid":null}
*/