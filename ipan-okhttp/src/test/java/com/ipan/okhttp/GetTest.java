package com.ipan.okhttp;

import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ipan.okhttp.http.DebugLoggingInterceptor;

/**
 * 普通GET请求测试
 * @author iPan
 * @version 2019-12-30
 */
public class GetTest {
	
	private static final String URL = "http://localhost/zchj_app/app/http-test/getTest.jsonx";
	
	@BeforeClass
	public static void init() {
		HttpClient.Instance.debugLog(DebugLoggingInterceptor.Level.ALL);
	}

	@Test
	public void getAsString() {
		String data = HttpClient
                .get(URL)
                .queryString("id", 999)
                .queryString("name", "张三")
                .asString();
		System.out.println("data=" + data);
	}
	
	@Test
	public void getAsMap() {
		Map<String, Object> map = HttpClient
				.get(URL)
                .queryString("id", 999)
                .queryString("name", "张三")
                .asMap();
		System.out.println("map=" + map);
	}
	
	@Test
	public void getAsBean() {
		Yhxx yhxx = HttpClient
				.get(URL)
                .queryString("id", 999)
                .queryString("name", "张三")
                .asBean(Yhxx.class);
		System.out.println("yhxx=" + yhxx);
	}
	
}
/*
getAsString:
2019-12-30 14:50:10,590 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Start to print the connection[http://localhost/zchj_app/app/http-test/getTest.jsonx?id=999&name=%E5%BC%A0%E4%B8%89] data ===|
2019-12-30 14:50:10,590 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Start to print request headers === |
2019-12-30 14:50:10,592 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Accept-Language : zh-CN,zh;q=0.8 ===|
2019-12-30 14:50:10,592 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== User-Agent : Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36 ===|
2019-12-30 14:50:10,592 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Finish to print request headers === |
2019-12-30 14:50:10,682 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== The request executes over,http statusCode is 200,http message is OK,taking 88 ms
2019-12-30 14:50:10,682 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Start to print response headers === |
2019-12-30 14:50:10,682 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Content-Language : zh-CN ===|
2019-12-30 14:50:10,683 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Content-Length : 874 ===|
2019-12-30 14:50:10,683 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Content-Type : text/html;charset=utf-8 ===|
2019-12-30 14:50:10,683 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Date : Mon, 30 Dec 2019 06:50:10 GMT ===|
2019-12-30 14:50:10,683 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Server : Apache-Coyote/1.1 ===|
2019-12-30 14:50:10,683 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Set-Cookie : JSESSIONID=D7BF350BEA1150CA7FE4ABF963C814B1; Path=/zchj_app; HttpOnly ===|
2019-12-30 14:50:10,683 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Finish to print response headers === |
2019-12-30 14:50:10,683 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Start to print response body === |
2019-12-30 14:50:10,684 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== The body data is 
{"createDate":null,"createTime":null,"updateDate":null,"updateTime":null,"statusCode":"200","message":"操作成功","image":null,"id":999,"zh":"demo","kl":null,"qx":null,"nc":null,"xm":"张三","xb":null,"cs":null,"whcd":null,"sfzh":null,"ysr":null,"szhy":null,"yx":null,"sj":null,"qq":null,"wxuid":null,"qquid":null,"wx":null,"wb":null,"tb":null,"xq":null,"zcsj":null,"lx":null,"bz":null,"fwq":null,"tpml":null,"dlsj":null,"dlcs":null,"khid":null,"tp0":null,"tp1":null,"tp2":null,"tp3":null,"tp4":null,"qy":null,"gxsj":null,"wxoid":null,"wxoid1":null,"wxoid2":null,"wxoid3":null,"wxoid4":null,"aliuid":null,"alilogin":null,"aliname":null,"szd":null,"szdName":null,"xgr":null,"xgsj":null,"tjr":null,"ddsj1":null,"uuid":null,"lastAppVer":null,"lastResVer":null,"platform":null,"sjsId":null,"sjsZt":null,"sfrzId":null,"sfrzZt":null,"jgrzId":null,"jgrzZt":null,"szdKhid":null}
2019-12-30 14:50:10,684 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Finish to print response body === |
2019-12-30 14:50:10,684 [main] INFO  [com.ipan.okhttp.http.DebugLoggingInterceptor] -  |=== Finish to print the connection[http://localhost/zchj_app/app/http-test/getTest.jsonx?id=999&name=%E5%BC%A0%E4%B8%89] data ===|

data={"createDate":null,"createTime":null,"updateDate":null,"updateTime":null,"statusCode":"200","message":"操作成功","image":null,"id":999,"zh":"demo","kl":null,"qx":null,"nc":null,"xm":"张三","xb":null,"cs":null,"whcd":null,"sfzh":null,"ysr":null,"szhy":null,"yx":null,"sj":null,"qq":null,"wxuid":null,"qquid":null,"wx":null,"wb":null,"tb":null,"xq":null,"zcsj":null,"lx":null,"bz":null,"fwq":null,"tpml":null,"dlsj":null,"dlcs":null,"khid":null,"tp0":null,"tp1":null,"tp2":null,"tp3":null,"tp4":null,"qy":null,"gxsj":null,"wxoid":null,"wxoid1":null,"wxoid2":null,"wxoid3":null,"wxoid4":null,"aliuid":null,"alilogin":null,"aliname":null,"szd":null,"szdName":null,"xgr":null,"xgsj":null,"tjr":null,"ddsj1":null,"uuid":null,"lastAppVer":null,"lastResVer":null,"platform":null,"sjsId":null,"sjsZt":null,"sfrzId":null,"sfrzZt":null,"jgrzId":null,"jgrzZt":null,"szdKhid":null}
*/
