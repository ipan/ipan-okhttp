package com.ipan.okhttp.exception;

/**
 * 配置HttpClient异常
 *
 * @author iPan
 * @version 2019-12-24
 */
public class HttpClientConfigException extends RuntimeException {

    private static final long serialVersionUID = 1788789757077983056L;

    public HttpClientConfigException(Throwable cause) {
        super(cause);
    }

    public HttpClientConfigException(String message) {
        super(message);
    }
}
