package com.ipan.okhttp.exception;

/**
 * Http Status Code exception,当code不是200时则会跑出本异常信息
 *
 * @author iPan
 * @version 2019-12-24
 */
public class HttpStatusCodeException extends HttpClientException {

    private static final long serialVersionUID = -1584716934177136972L;
    private final String url;
    private final int statusCode;
    private final String statusMessage;

    public HttpStatusCodeException(String url, int statusCode, String statusMessage) {
        super("Request url[=" + url + "] failed, status code is " + statusCode + ",status message is " + statusMessage);
        this.url = url;
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
    }

    /**
     * 请求失败时的HTTP Status Code
     *
     * @return Http错误码
     */
    public int getStatusCode() {
        return statusCode;
    }

    /**
     * 请求失败时错误消息
     *
     * @return 失败消息
     */
    public String getStatusMessage() {
        return statusMessage;
    }

    /**
     * 返回请求地址
     *
     * @return 请求地址
     */
    public String getUrl() {
        return url;
    }
}
