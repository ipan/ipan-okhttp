package com.ipan.okhttp.request;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.lang3.Validate;

import com.ipan.kits.io.IOUtil;
import com.ipan.okhttp.utils.Utils;

import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * <p>
 * POST提交二进制流，服务端应该从Request请求体获取二进制流。
 * </p>
 *
 * @author iPan
 * @version 2019-12-25
 */
public class BinaryBodyPostRequest extends BaseBodyHttpRequest<BinaryBodyPostRequest> {

    /**
     * 二进制流内容
     */
    private byte[] content;
    private MediaType mediaType;

    /**
     * 默认构造器
     *
     * @param url 请求地址
     */
    public BinaryBodyPostRequest(String url) {
        super(url);
    }


    /**
     * 设置二进制流
     *
     * @param inputStream 二进制流
     * @return {@link BinaryBodyPostRequest}
     * @see MediaType
     */
    public BinaryBodyPostRequest stream(InputStream inputStream) {
    	Validate.notNull(inputStream, "In must not be null.");
    	Validate.notNull(mediaType, "MediaType must not be null.");
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            if (IOUtil.copy(inputStream, outputStream) == -1) {
                throw new IOException("Copy failed");
            }
            this.content = outputStream.toByteArray();
        } catch (IOException e) {
            throw new RuntimeException("Reading stream failed->", e);
        } finally {
            IOUtil.closeQuietly(inputStream);
        }
        return this;
    }

    /**
     * 设置文件，转为文件流
     *
     * @param file 文件对象
     * @return {@link BinaryBodyPostRequest}
     */
    public BinaryBodyPostRequest file(File file) {
    	Validate.notNull(file, "File must not be null.");
        String filename = file.getName();
        this.mediaType = Utils.guessMediaType(filename);
        try {
            this.stream(new FileInputStream(file));
            return this;
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 设置请求内容类型
     *
     * @param contentType 请求内容类型
     * @return {@link BinaryBodyPostRequest}
     */
    public BinaryBodyPostRequest contentType(String contentType) {
    	Validate.notBlank(contentType, "ContentType must not be null.");
        this.mediaType = MediaType.parse(contentType);
        return this;
    }

    /**
     * 获取{@linkplain RequestBody}对象
     */
    @Override
    protected RequestBody generateRequestBody() {
        return RequestBody.create(this.mediaType, this.content);
    }

}
