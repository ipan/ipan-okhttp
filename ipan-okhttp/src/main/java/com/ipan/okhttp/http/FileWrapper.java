package com.ipan.okhttp.http;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.lang3.Validate;

import com.ipan.kits.base.ExceptionUtil;
import com.ipan.okhttp.utils.Utils;

import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * 文件包装类
 *
 * @author iPan
 * @version 2019-12-25
 */
public class FileWrapper {

    private InputStream content;
    private File file;
    private String filename;
    private MediaType mediaType;

    FileWrapper(Builder builder) {
        this.mediaType = builder.mediaType;
        this.file = builder.file;
        this.filename = builder.filename;
        this.content = builder.content;
    }

    public String getFilename() {
        return filename;
    }

    public static Builder create() {
        return new Builder();
    }

    public RequestBody requestBody() {
        if (this.file != null) {
            return new FileRequestBody(this.file, this.mediaType);
        }
        InputStreamRequestBody ir = null;
		try {
			ir = new InputStreamRequestBody(this.content, this.mediaType);
		} catch (IOException e) {
			ExceptionUtil.unchecked(e);
		}
        return ir;
    }

    public static class Builder {
        private InputStream content;
        private File file;
        private String filename;
        private MediaType mediaType;

        public Builder file(File file) {
            Validate.notNull(file, "File may not be null.");
            if (!file.exists()) {
                throw new RuntimeException("File does not exist.");
            }
            this.file = file;
            return this;
        }

        public Builder filename(String filename) {
        	Validate.notBlank(filename, "Filename may not be null.");
            this.filename = filename;
            return this;
        }

        public Builder stream(InputStream stream) {
        	Validate.notNull(stream, "Stream may not be null.");
            this.content = stream;
            return this;
        }

        public Builder contentType(String contentType) {
        	Validate.notBlank(contentType, "ContentType may not be null.");
            this.mediaType = MediaType.parse(contentType);
            return this;
        }

        public Builder mediaType(MediaType mediaType) {
        	Validate.notNull(mediaType, "Media may not be null.");
            this.mediaType = mediaType;
            return this;
        }

        public FileWrapper build() {
            if (this.file != null) {
                if (this.filename == null) {
                    this.filename = file.getName();
                }
            } else if (this.content != null) {
                if (this.filename == null) {
                    throw new RuntimeException("Filename may not be null");
                }
            } else {
                throw new RuntimeException("The content is null.");
            }
            if (this.mediaType == null) {
                this.mediaType = Utils.guessMediaType(this.filename);
            }
            return new FileWrapper(this);
        }

    }
}
