package com.ipan.okhttp.response.handle;

import java.io.IOException;

/**
 * 数据处理定义接口,将得到的响应结果转为所需的数据
 *
 * @author iPan
 * @version 2019-12-24
 * 
 * @see JsonDataHandler
 * @see StringDataHandler
 * @see FileDataHandler
 */
public interface DataHandler<T> {

    /**
     * 得到相应结果后,将相应数据转为需要的数据格式
     *
     * @param response 需要转换的对象
     * @return 转换结果
     * @throws IOException 出现异常
     */
    T handle(final okhttp3.Response response) throws IOException;

}
