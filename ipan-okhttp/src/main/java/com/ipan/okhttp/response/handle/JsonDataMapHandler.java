package com.ipan.okhttp.response.handle;

import java.io.IOException;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.ipan.kits.mapper.JsonMapper;

import okhttp3.Response;

/**
 * JSON处理器,将得到的JSON字符串转为Map.
 *
 * @author iPan
 * @version 2019-12-25
 */
public class JsonDataMapHandler implements DataHandler<Map<String, Object>> {

	/**
	 * 得到相应结果后,将相应数据转为需要的数据格式
	 *
	 * @param response 需要转换的对象
	 * @return 转换结果
	 * @throws IOException 出现异常
	 */
	@Override
	public Map<String, Object> handle(final Response response) throws IOException {
		StringDataHandler stringDataHandler = StringDataHandler.create();
		String valueContent = stringDataHandler.handle(response);
		if (StringUtils.isNotBlank(valueContent)) {
			return JsonMapper.getDefaultJsonMapper().jsonToMap(valueContent);
		}
		return null;
	}
}
