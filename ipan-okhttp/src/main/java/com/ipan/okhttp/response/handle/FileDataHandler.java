package com.ipan.okhttp.response.handle;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

import com.ipan.kits.io.IOUtil;
import com.ipan.okhttp.utils.Utils;

import okhttp3.Response;

/**
 * 文件处理器,一般用于从远程下载资源(如图片、报表等)
 * 支持自动获取文件名，也支持自定义文件名
 *
 * @author iPan
 * @version 2019-12-24
 */
public class FileDataHandler implements DataHandler<File> {

    /**
     * 保存的文件目录
     */
    private final String dirPath;

    /**
     * 保存的文件名
     */
    private String filename;

    public FileDataHandler(String dirPath) {
    	Validate.notBlank(dirPath, "DirPath may not be null.");
        this.dirPath = dirPath;
    }

    public FileDataHandler(String dirPath, String filename) {
        this(dirPath);
        this.filename = filename;
    }

    /**
     * 返回保存的文件目录
     *
     * @return 保存的文件目录
     */
    public String getDirPath() {
        return dirPath;
    }

    /**
     * 返回保存的文件名
     *
     * @return 保存的文件名
     */
    public String getFilename() {
        return filename;
    }

    /**
     * 设置保存的文件名
     *
     * @param filename 保存的文件名
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }

    /**
     * 得到相应结果后,将相应数据转为需要的数据格式
     *
     * @param response 需要转换的对象
     * @return 存储的文件信息
     * @throws IOException 出现异常
     */
    @Override
    public File handle(final Response response) throws IOException {
        String name = this.filename;
        if (StringUtils.isBlank(name)) name = Utils.getFilename(response);
        File saveFile = new File(this.dirPath, name);
        FileOutputStream fout = IOUtil.openFileOutputStream(saveFile);
        try {
			IOUtil.copy(response.body().byteStream(), fout);
		} finally {
			if (fout != null) {
				IOUtil.closeQuietly(fout);
			}
		}
        return saveFile;
    }
}
